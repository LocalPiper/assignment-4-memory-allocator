#include "tests.h"
#include "mem.h"
#include <stdio.h>

static void test1() {
  printf("TEST 1\n");
  heap_init(1);

  void *alloc = _malloc(64);

  debug_heap(stdout, HEAP_START);
  _free(alloc);

  munmap(HEAP_START, 8192);
  printf("\nTEST 1 PASSED\n");
}

static void test2() {
  printf("TEST 2\n");
  heap_init(1);

  void *alloc = _malloc(32);
  void *alloc2 = _malloc(64);

  debug_heap(stdout, HEAP_START);
  _free(alloc);

  debug_heap(stdout, HEAP_START);
  _free(alloc2);

  munmap(HEAP_START, 8192);
  printf("\nTEST 2 PASSED\n");
}

static void test3() {
  printf("TEST 3\n");
  heap_init(1);

  debug_heap(stdout, HEAP_START);

  void *alloc = _malloc(8192);

  debug_heap(stdout, HEAP_START);
  _free(alloc);

  munmap(HEAP_START, 20480);
  printf("\nTEST 3 PASSED\n");
}

void run_tests() {
  test1();
  test2();
  test3();
  printf("\nALL TESTS PASSED\n");
}
